#include "inireader.h"
#include <iostream>

IniReader::IniReader() {
}

void IniReader::printUsage() {
    std::cout<<"INI Settings Reader"<<std::endl;
    std::cout<<"Usage: "<<"utility_name -f <ini file> [-s section] [-p parameter]"<<std::endl;
    std::cout<<"-f\t\t <ini file>\tpath to ini file"<<std::endl;
    std::cout<<"-s\t\t <section>\tsection of ini file"<<std::endl;
    std::cout<<"-p\t\t <parameter>\tparameter of ini file"<<std::endl;
    std::cout<<"-h\t\t show this help"<<std::endl;
    std::cout<<"?\t\t show this help"<<std::endl;
}

bool IniReader::parseAction( int argc, char * const argv[] ) {
    short action = NOACTION;
    if( parseCommand( argc, argv, action ) == 0 ) return false;

    QSettings settings(m_filename.c_str(), QSettings::IniFormat);

    if( action == NOACTION ) {
        printError("Action not given");
        return false;
    }
    else if( hasFlag(action, SECTION) && !hasFlag(action, VALUE) ) {
        if( !checkParam(m_section) ) {
            printError("Section value not given");
            return false;
        }
        readSection( settings );
    }
    else if( !hasFlag(action, SECTION) && hasFlag(action, VALUE) ) {
        if( !checkParam(m_parameter) )
        {
            printError("Parameter value not given");
            return false;
        }
        readParameter( settings, m_parameter.c_str() );
        return true;
    }
    else if(hasFlag(action, SECTION) && hasFlag(action, VALUE))
    {
        if( !checkParam(m_parameter) || !checkParam(m_section) ) {
            printError("Section/parameter value not given");
            return false;
        }
        QString parameter = QString("%1/%2").arg(m_section.c_str(), m_parameter.c_str());
        readParameter( settings, parameter );

        return true;
    }
    else
    {
        printError("Wrong parameters");
    }
    return true;
}

void IniReader::readParameter( QSettings& settings, const QString& parameter )
{
    QVariant default_value("");
    if(settings.contains(parameter)) {
        std::cout << settings.value(parameter, default_value).toString().toStdString() << std::endl;
    }
}

void IniReader::readSection( QSettings& settings )
{
    QStringList sections = settings.childGroups();
    std::map<QString, QString> map;
    if( sections.contains(m_section.c_str(), Qt::CaseInsensitive) ){
        settings.beginGroup(m_section.c_str());
        QStringList keys = settings.childKeys();
        std::cout << QString("[%1]").arg(m_section.c_str()).toStdString() << std::endl;
        foreach (QString key, keys) {
            std::cout << key.toStdString() << " = " << settings.value(key).toString().toStdString() << std::endl;
        }
        settings.endGroup();
    }
}

bool IniReader::hasFlag ( const short& a, const short& b )
{
    return (a & b) == b;
}

int IniReader::parseCommand( int argc, char * const argv[], short int& actionMask ) {
    int ret = 1;
    char opt;
    while (( opt = getopt( argc, argv, "f:s:p:h" )) != -1 ) {
        switch (opt) {
        case 'f':
            m_filename = optarg;
            break;
        case 's':
        {
            actionMask |= SECTION ;
            m_section.assign( optarg );
            break;
        }
        case 'p':
        {
            actionMask |= VALUE;
            m_parameter.assign( optarg );
            break;
        }
        case 'h':
            ret = 0;
            printUsage();
            break;
        default: /* '?' */
            ret = 0;
            printUsage();
            break;
        }
    }
    return ret;
}

bool IniReader::checkParam( const std::string& parameter ) {
    bool ret = false;
    if( ! parameter.empty() )
        ret = true;
    else
        std::cerr << "Parameter not given" << std::endl;
    return ret;
}

void IniReader::printError( const std::string & error ) {
    std::cerr << error << std::endl;
    printUsage();
}
