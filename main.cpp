#include <unistd.h>
#include "inireader.h"


int main(int argc, char *argv[])
{
    int ret = -1;
    IniReader* pReader = new IniReader();

    pReader->parseAction(argc, argv);

    if(pReader)
        delete pReader;

    return ret;
}
