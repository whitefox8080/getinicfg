#ifndef INIREADER_H
#define INIREADER_H
#include <QSettings>
#include <unistd.h>

enum actionKind {
    NOACTION    = 1,
    SECTION     = 2,
    VALUE       = 4
};

inline actionKind operator|(actionKind a, actionKind b)
{return static_cast<actionKind>(a | b);}

class IniReader
{
public:
    IniReader();
    bool parseAction( int argc, char * const argv[] );
protected:
    void printUsage();
    int  parseCommand( int argc, char * const argv[], short int& actionMask );
    bool checkParam( const std::string& );
    bool hasFlag ( const short&, const short& );
    void printError( const std::string& );
    void readSection( QSettings& );
    void readParameter( QSettings&, const QString& );
private:
    std::string m_filename;
    std::string m_section;
    std::string m_parameter;
};

#endif // INIREADER_H
